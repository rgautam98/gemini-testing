import os
import vertexai
from vertexai.generative_models import GenerativeModel, Part

from llm_functions import getLlmFunctions

system_instruction_file = open('system_prompt.txt', 'r')
system_instruction = system_instruction_file.read()
system_instruction_file.close()

generation_config = {
    "max_output_tokens": 50000,
    "temperature": 1,
    "top_p": 0.95,
}


vertexai.init(project=os.environ['GOOGLE_PROJECT_ID'], location=os.environ['GOOGLE_LOCATION'])
model = GenerativeModel(
  "gemini-1.5-pro-preview-0409",
  system_instruction=[system_instruction],
  tools=[getLlmFunctions()]
)

def getModel():
  return model

