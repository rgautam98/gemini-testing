import json
from google.protobuf.json_format import MessageToJson

data_path = 'itineraries/bom-jfk.json'

def doApiCall():
    # Technically we should do an api call but we are skipping that for the time being
    json_text = open(data_path).read()
    data = json.loads(json_text)
    return data

def generate_itinerary(func_data):
    print("Function: Generating itinerary b/w {} and {} on {}".format(func_data['source'], func_data['destination'], func_data['date']))
    api_response = doApiCall()
    carriers = api_response["dictionaries"]["carriers"]
    aircraft = api_response["dictionaries"]["aircraft"]
    iteneries = ""

    for i in api_response["data"]:
        itenery = "--------------------------------------------------------------------------------\n"
        itenery += f"Seats Available: {i['numberOfBookableSeats']}\n"
        itenery += f"Price: {i['price']['currency']} {i['price']['total']}\n"
        
        for j in i['itineraries']:
            itenery += ''
            j_count = 0
            for k in j['segments']:
                itenery += f"{k['departure']['iataCode']} T{k['departure'].get('terminal') if k['departure'].get('terminal') else ''} {k['departure']['at']} -> {k['arrival']['iataCode']} T{k['arrival'].get('terminal') if k['arrival'].get('terminal') else ''} {k['arrival']['at']}\n"
                itenery += f"Duration: {k['duration'].split('PT')[1]}\n"
                itenery += f"Carrier: {carriers[k['carrierCode']]} {k['carrierCode']}{k['number']}\n"
                itenery += f"Aircraft: {aircraft[k['aircraft']['code']]}\n"

                j_count += 1
                if len(j['segments']) != j_count: itenery += "+++++\n"
        
        iteneries += itenery
    return iteneries


def book_tickets(func_data):
    print("Function: Booking tickets for the following itenery")
    print(func_data)
    return "Tickets booked successfully"

def functionDict():
    return {
        "generate_itinerary": generate_itinerary,
        "book_tickets": book_tickets
    }

def main():
    itenery = generate_itinerary({'source': "BOM", 'destination':  "JFK", 'date': "2024-05-12"})
    print(itenery)

if __name__ == "__main__":
    main()