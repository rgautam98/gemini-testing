
from vertexai.generative_models import (
    FunctionDeclaration,
    Tool,
)

generate_itinerary = FunctionDeclaration(
    name="generate_itinerary",
    description= 'Returns the available itinerary for a given source, destination and date',
    parameters={
        "type": "object",
        "properties": {
            "source": {"type": 'string', "description": "The source airport code."},
            "destination": {"type": 'string', "description": "The destination airport code"},
            "date": {"type": 'string', "description": "Date of journey in YYYY-MM-DD format"} 
            },
        "required": ["source", "destination", "date"]
    },
)

book_tickets = FunctionDeclaration(
   name="book_tickets",
   description = "Book tickets for a given itinerary",
   parameters={
       "type": "object",
       "properties": {
          "seats_available": {"type": "string", "description": "Seats available for a flight"},
          "price": {"type": "string", "description": "Price of the ticket"},
          "total_travel_time": {"type": "string", "description": "The total travel time"},
           "itinerary": {
               "type": "array",
               "description": "All flights included in the itinerary",
               "items": {
                   "description": "Data for for an individual flight in the itinerary",
                   "type": "object",
                   "properties": {
                        "airport": {"type": "string", "description": "IATA code of the starting airport"},
                        "terminal": {"type": "string", "description": "Departure terminal of the airport"},
                        "destination": {"type": "string", "description": "IATA code of the destination airport"},
                        "destination_terminal": {"type": "string", "description": "Arrival terminal of the destination airport"},
                        "date": {"type": "string", "description": "Date of departure"},
                        "time": {"type": "string", "description": "Time of departure"},
                        "flight_duration": {"type": "integer", "description": "Duration of flight"},
                        "carrier": {"type": "string", "description": "Carrier Name"},
                        "flight_number": {"type": "string", "description": "Flight number"},
                        "aircraft": {"type": "string", "description": "Aircraft type"},
                   },
                   "required": ["airport", "terminal", "destination", "destination_terminal", "date", "time", "flight_duration", "carrier", "flight_number", "aircraft"],
               },
           },
       },
       "required": ["flights", 'total_travel_time', 'price'],
   },
)

def getLlmFunctions():
    return Tool(
        function_declarations=[generate_itinerary, book_tickets],
    )

def main():
    print(getLlmFunctions())

if __name__ == "__main__":
    main()
