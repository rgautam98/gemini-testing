# LLM Flight Planner 

This is an flight planner that showcases the power of Gemini by Google Cloud. 

The idea behind the app is that you are presented with a chat interface where you can ask for a flight from one city to another. The app will then use the Gemini API to find the best flight for you.

It uses Amadeus developer api to get the flight data. For the sake of the example, it does not actually call the api. Rather the source and destination are hardcoded with BOM to JFK. 

The chat interface will look something like this

![chat replay](images/ss.png)


The code shows the power of Gemini with the following features:
- Using LLM to get the best flight from a list of available flights
- Function calling to enrich data
- Function calling to send data out of LLM in a structured format

