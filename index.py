from generate_itenery import functionDict
from llm import getModel
from vertexai.generative_models import Part
import vertexai.preview.generative_models as generative_models
import traceback

safety_settings = {
    generative_models.HarmCategory.HARM_CATEGORY_HATE_SPEECH: generative_models.HarmBlockThreshold.BLOCK_NONE,
    generative_models.HarmCategory.HARM_CATEGORY_DANGEROUS_CONTENT: generative_models.HarmBlockThreshold.BLOCK_NONE,
    generative_models.HarmCategory.HARM_CATEGORY_SEXUALLY_EXPLICIT: generative_models.HarmBlockThreshold.BLOCK_NONE,
    generative_models.HarmCategory.HARM_CATEGORY_HARASSMENT: generative_models.HarmBlockThreshold.BLOCK_NONE,
}


py_functions = functionDict()


def callFunctionOrPrintResponse(response):
    if hasattr(response.candidates[0].content.parts[0], 'text'):
        print(f"Model: {response.candidates[0].content.parts[0].text}")
        return ''
    else:
        llm_response = resolveFunctionCall(response.candidates[0].content.parts[0].function_call)
        return llm_response

def getFunctionParameters(functionCall):
    response = {}
    for param in functionCall.args:
          response[param] = functionCall.args[param]
    print(response)
    return response
    

def resolveFunctionCall(functionCall):
  function_params = getFunctionParameters(functionCall)
  function_response = py_functions[functionCall.name](function_params)
  llm_response = Part.from_function_response(
    name = functionCall.name,
    response = {
        "content": function_response,
    }
  )
  return llm_response


def startChat(convo):
   llm_response = ''
   while True:
        
    try: 
      if llm_response == '':
        user_input = input("You: ")
        response = convo.send_message(user_input, safety_settings=safety_settings)
      else:
          response = convo.send_message(llm_response, safety_settings=safety_settings)
      llm_response = callFunctionOrPrintResponse(response)
      print('-----------------------------------')
    except Exception as e:
        print(e)
        traceback.print_exc()
        print('-----------------------------------')
        print(llm_response)
        print('-----------------------------------')
        print(response)
        print('-----------------------------------')
        llm_response=''
        print("There was an issue in processing your request. Please try again.")
   

def main():
  model = getModel()
  convo = model.start_chat(response_validation=False)
  # convo.send_message("Can you check flights from BOM to JFK for 12th May?")
  startChat(convo)


if __name__ == '__main__':
    main()